#ifndef LABELS_H
#define LABELS_H
typedef struct lookup* lookuptable_t;
struct lookup {
	char *key;
	int id;
	int addr;
	struct lookup *next;
};

int label_lookup_or_create(lookuptable_t *t, char *key);
int label_lookup_addr(lookuptable_t t, int id);
char *label_lookup_name(lookuptable_t t, int id);
void label_fill_addr(lookuptable_t *t, char *key, int addr);
int label_append(lookuptable_t *t, char *key, int addr);
void label_destroy(lookuptable_t t);


typedef struct reloc* reloctable_t;
struct reloc {
	int id;
	int instruction_index;
	struct reloc *next;
};

void reloc_append(reloctable_t *t, int id, int instruction_index);
void reloc_destroy(reloctable_t t);

#endif
