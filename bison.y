%{
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "aqa.h"
#include "labels.h"

int yyerror(const char *s);
int yylex();
int instruction_index = 0;

void append_instruction(uint64_t i){
	memory[instruction_index] = i;
	instruction_index++;
}

void new_instruction(char mod, char condition, char opcode, char reg1, char reg2, int imm){
	uint64_t i = (((uint64_t)mod << MOD_OFFSET) & MOD_MASK)
		| (((uint64_t)condition << COND_OFFSET) & COND_MASK)
		| (((uint64_t)opcode << OPCODE_OFFSET) & OPCODE_MASK)
		| (((uint64_t)reg1 << REG1_OFFSET) & REG1_MASK)
		| (((uint64_t)reg2 << REG2_OFFSET) & REG2_MASK)
		| (((uint64_t)imm << IMM_OFFSET) & IMM_MASK);
	append_instruction(i);
}

%}

%define parse.error verbose

%union {
	int number;
	struct {
		char opcode;
		char condition;
	}action;
	char *str;
	struct {
		char mod;
		int64_t value;
		int reg_offset;
	}argument;
}

%token <number> TOK_NUMERIC TOK_REG
%token <action> TOK_INS_REG_OP TOK_INS_REG_NUM TOK_INS_NO_ARG TOK_INS_ADDR TOK_INS_REG_REG_OP TOK_INS_REG TOK_INS_REG_ADDR
%token <str> TOK_LABEL
%token TOK_SEP TOK_NUMERIC_PREFIX TOK_BRACKET_OPEN TOK_BRACKET_CLOSE TOK_NEWLINE TOK_SEMICOLON TOK_COLON TOK_PLUS TOK_MINUS TOK_DATA TOK_DOT
%type <argument> operand addr_ref
%type <number> addr_fixed
%%
grammar:
			 %empty
			 | grammar inst instruction_sep
			 | grammar TOK_NEWLINE
			 | grammar label
			 | grammar data
			 | error;
inst:
		TOK_INS_REG_OP TOK_REG TOK_SEP operand {
			new_instruction(
				$4.mod,
				$1.condition,
				$1.opcode,
				$2,
				0,
				$4.value);
		} | TOK_INS_REG_NUM TOK_REG TOK_SEP TOK_NUMERIC {
			new_instruction(
				0,
				$1.condition,
				$1.opcode,
				$2,
				0,
				$4);
		} | TOK_INS_NO_ARG {
			new_instruction(
				0,
				$1.condition,
				$1.opcode,
				0,
				0,
				0);
		} | TOK_INS_REG_ADDR TOK_REG TOK_SEP addr_ref {
			new_instruction(
				$4.mod,
				$1.condition,
				$1.opcode,
				$2,
				$4.reg_offset,
				$4.value);
		} | TOK_INS_ADDR addr_ref {
			new_instruction(
				$2.mod,
				$1.condition,
				$1.opcode,
				$2.reg_offset,
				0,
				$2.value);
		} | TOK_INS_REG_REG_OP TOK_REG TOK_SEP TOK_REG TOK_SEP operand {
			new_instruction(
				$6.mod,
				$1.condition,
				$1.opcode,
				$2,
				$4,
				$6.value);
		} | TOK_INS_REG TOK_REG {
			new_instruction(
				0,
				$1.condition,
				$1.opcode,
				$2,
				0,
				0);
		}

data:
		TOK_DATA TOK_NUMERIC {
			append_instruction($2);
		}

label:
		 TOK_LABEL TOK_COLON {
			label_fill_addr(&labels_lookup, $1, instruction_index);
		 }

instruction_sep: TOK_NEWLINE|TOK_SEMICOLON

operand:
	 TOK_NUMERIC_PREFIX addr_fixed{
		$$.value = $2;
		$$.mod = 0;
	 } | TOK_REG{
		$$.value = $1;
		$$.mod = 1;
	 }
addr_ref:
		addr_fixed{
			$$.value = $1;
			$$.reg_offset = 0;
			$$.mod = 0;
		} | TOK_BRACKET_OPEN TOK_REG TOK_BRACKET_CLOSE {
			$$.value = $2;
			$$.reg_offset = 0;
			$$.mod = 1;
		} | TOK_BRACKET_OPEN TOK_REG TOK_PLUS addr_fixed TOK_BRACKET_CLOSE {
			$$.value = $4;
			$$.mod = 2;
			$$.reg_offset = $2;
		}
addr_fixed:
					TOK_NUMERIC{
						$$ = $1;
					} | TOK_LABEL {
						int id = label_lookup_or_create(&labels_lookup, $1);
						reloc_append(&labels_reloc, id, instruction_index);
						$$ = 0;
					} | TOK_DOT TOK_PLUS TOK_NUMERIC {
						$$ = instruction_index + $3;
					} | TOK_DOT TOK_MINUS TOK_NUMERIC {
						$$ = instruction_index - $3;
					} | TOK_DOT {
						$$ = instruction_index;
					}

%%
int yyerror(const char *s){
	fprintf(stderr,"yyerror before instruction %d: %s \n",instruction_index,s);
	return 0;
}
