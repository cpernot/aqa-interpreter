# AQA Interpreter
## Dependencies
* Flex
* Bison
* A C compiler
## Installation
### Install dependencies
Debian-based distributions:
```sh
sudo apt install flex bison gcc
```
Arch-based distributions:
```sh
sudo pacman -S flex bison gcc
```
### Download source and compile
```sh
git clone https://gitlab.com/cpernot/aqa-interpreter.git
cd aqa-interpreter
make
```
## Usage
```sh
./aqa FILE
```
Where `FILE` is the path to you program.
