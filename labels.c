#include <stdlib.h>
#include <string.h>
#include "labels.h"

int label_lookup_or_create(lookuptable_t *t, char *key) {
	for (lookuptable_t lookup = *t;lookup;lookup=lookup->next)
		if (!strcmp(lookup->key,key))
			return lookup->id;
	return label_append(t, key, -1);
}

void label_fill_addr(lookuptable_t *t, char *key, int addr){
	for (lookuptable_t lookup = *t;lookup;lookup=lookup->next){
		if (!strcmp(lookup->key,key)){
			lookup->addr = addr;
			return;
		}
	}
	label_append(t, key, addr);
}

int label_lookup_addr(lookuptable_t t, int id){
	for (lookuptable_t lookup = t;lookup;lookup=lookup->next)
		if (lookup->id == id)
			return lookup->addr;
	return -1;
}

char *label_lookup_name(lookuptable_t t, int id){
	for (lookuptable_t lookup = t;lookup;lookup=lookup->next)
		if (lookup->id == id)
			return lookup->key;
	return 0;
}

int label_append(lookuptable_t *t, char *key, int addr){
	struct lookup *new_element = malloc(sizeof(struct lookup));
	new_element->id = *t ? (*t)->id + 1: 0;
	new_element->key = strdup(key);
	new_element->addr = addr;
	new_element->next = *t;
	*t = new_element;
	return new_element->id;
}

void label_destroy(lookuptable_t t){
	for (lookuptable_t lookup = t;lookup;lookup=lookup->next)
		free(lookup);
}

void reloc_append(reloctable_t *t, int id, int instruction_index){
	struct reloc *new_element = malloc(sizeof(struct reloc));
	new_element->id = id;
	new_element->instruction_index = instruction_index;
	new_element->next = *t;
	*t = new_element;
}

void reloc_destroy(reloctable_t t){
	for (reloctable_t reloc = t;reloc;reloc=reloc->next)
		free(reloc);
}
