CC = gcc
BISON = bison
FLEX = flex
PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
CFLAGS = -O3
HEADERS = aqa.h bison.h config.h labels.h
BISON_REQUIRED_HEADERS = aqa.h labels.h
OBJECTS = bison.o aqa.o parser.o labels.o
TEMP = ${OBJECTS} bison.c bison.h parser.c
BIN = aqa

all: ${BIN}

install: ${BIN}
	mkdir -p -- ${DESTDIR}${BINDIR}
	cp -f -- ${BIN} ${DESTDIR}${BINDIR}

aqa: ${OBJECTS} bison.h
	${CC} ${OBJECTS} -o $@

.c.o:
	${CC} -c ${CFLAGS} $< -o $@

bison.h bison.c: bison.y ${BISON_REQUIRED_HEADERS}
	${BISON} -o bison.c $< --defines=bison.h

parser.c: parser.l ${HEADERS}
	${FLEX} -o $@ $<

uninstall:
	rm -f ${DESTDIR}${BINDIR}/${BIN}

clean:
	rm -f ${BIN} ${TEMP}
