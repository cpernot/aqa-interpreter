#ifndef AQA_H
#define AQA_H
#include <inttypes.h>

#include "labels.h"

#define MOD_MASK    0xF000000000000000UL
#define COND_MASK   0x0F00000000000000UL
#define OPCODE_MASK 0x00FF000000000000UL
#define REG1_MASK   0x0000FF0000000000UL
#define REG2_MASK   0x000000FF00000000UL
#define IMM_MASK    0x00000000FFFFFFFFUL

#define MOD_OFFSET 60
#define COND_OFFSET 56
#define OPCODE_OFFSET 48
#define REG1_OFFSET 40
#define REG2_OFFSET 32
#define IMM_OFFSET 0

#define FLAG_V (1<<0)
#define FLAG_C (1<<1)
#define FLAG_Z (1<<2)
#define FLAG_N (1<<3)

enum condition {
	COND_LT = 1,
	COND_EQ,
	COND_LE,
	COND_GT,
	COND_NE,
	COND_GE,
};
enum actions {
	INS_MOV = 1,
	INS_MVN,
	INS_LDR,
	INS_STR,
	INS_INP,
	INS_OUT,
	INS_HALT,
	INS_CMP,
	INS_B,
	INS_ADD,
	INS_SUB,
	INS_MUL,
	INS_DIV,
	INS_REM,
	INS_AND,
	INS_ORR,
	INS_EOR,
	INS_LSL,
	INS_LSR,
	INS_CALL,
	INS_RET,
	INS_PUSH,
	INS_POP,
	INS_MAX
};

extern int *registers;
extern int64_t *memory;
extern lookuptable_t labels_lookup;
extern reloctable_t labels_reloc;
#endif
