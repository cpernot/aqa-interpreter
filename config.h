#ifndef CONFIG_H
#define CONFIG_H

//#define DEBUG

#define DEFAULT_REGISTERS_NUMBER 13 // up to 256
#define DEFAULT_STACK_SIZE (8*1024)
#define DEFAULT_MEMORY_SIZE (2*1024)


/* If enabled, you can write conditions after all
 * instructions.
 *
 * For instance:
 *	`if (r1 < 5) r0 = 3; else r0 = 2;`
 *
 * can be written in 3 instructions:
 *	mov r0,#2
 * 	cmp r1,#5
 * 	movge r0,#3
 *
 * instead of 5 instructions:
 *	cmp r1,#5
 *	bge else
 *	mov r0,#3
 *	b endif
 * else:
 *	mov r0,#2
 * endif:
 *
 * However, if you enable it, all instructions
 * except conditional jumps will be a little
 * bit slower.
 * 
 * Would you trade performance for convenience ?
 */
#define ALL_INSTRUCTIONS_ARE_CONDITIONAL

#endif
