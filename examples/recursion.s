mov r0,#38
call fib
out r4,4 // affiche 13
halt

//int fib(int n){
//  if (n<=1)
//    return 1;
//  return fib(n-1)+fib(n-2)
//}

fib:
push r0
push r3
cmp r0,#1
movle r4,#1
ble return
sub r0,r0,#1
call fib
mov r3,r4
sub r0,r0,#1
call fib
add r4,r4,r3
return:
pop r3
pop r0
ret
