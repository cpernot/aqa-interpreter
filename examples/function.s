mov r0,#0
mov r1,#0
mov r2,#1

mov r3,#0
loop:
call print
call step
add r3,r3,#1
cmp r3,#12
blt loop
halt

step:
mov r0,r1
mov r1,r2
add r2,r1,r0
ret

print:
out r2,4
ret
