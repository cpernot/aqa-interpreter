mov r1,#1
call function
out r1,4 // la fonction n'a pas modifié r1
halt

function:
//push permet de sauvegarder r1
push r1
mov r1,#4
//pop permet de le restaurer
pop r1
ret
