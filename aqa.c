#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "aqa.h"
#include "bison.h"
#include "labels.h"
#include "config.h"

#define GETFLAG(f) ((flags & f)>0)

extern FILE *yyin;
extern int instruction_index;
lookuptable_t labels_lookup;
reloctable_t labels_reloc;
int *registers;
int64_t *memory;
int *stack;
int stack_index = 0;
int pc = 0;
int running = 1;
int flags = 0;

static int parsefile(const char *path){
	FILE *conf = fopen(path,"r");
	if (conf == NULL){
		return -1;
	}
	yyin = conf;
	yyparse();
	fclose(conf);
	yyin = stdin;
	return 0;
}

void resolve_labels(){
	for (reloctable_t current = labels_reloc;current;current=current->next){
		int addr = label_lookup_addr(labels_lookup,current->id);
		int64_t i = memory[current->instruction_index];
		i &= ~IMM_MASK;
		i |= (addr<< IMM_OFFSET) & IMM_MASK;
		char *label_name = label_lookup_name(labels_lookup, current->id);
		memory[current->instruction_index] = i;
	}
}

int main(int argc, char **argv) {
	int64_t instruction;
	char *format;
	char mod, condition, opcode, reg1, reg2;
	int imm, val1, val2, n, d, b, m;
	int registers_number=DEFAULT_REGISTERS_NUMBER;
	int stack_size=DEFAULT_STACK_SIZE;
	int memory_size=DEFAULT_MEMORY_SIZE;
	memory = calloc(memory_size, sizeof(long));
	registers = calloc(registers_number, sizeof(int));
	stack = calloc(stack_size, sizeof(int));
	if (argc != 2){
		fprintf(stderr, "usage: %s file.s\n", argv[0]);
		return 1;
	}
	if (parsefile(argv[1]) < 0){
		fputs("Can't parse the file \n", stderr);
		return 1;
	}
	resolve_labels();
	while (running){
		instruction = memory[pc];
		pc++;
		mod = (instruction & MOD_MASK) >> MOD_OFFSET;
		condition = (instruction & COND_MASK) >> COND_OFFSET;
		opcode = (instruction & OPCODE_MASK) >> OPCODE_OFFSET;
		reg1 = (instruction & REG1_MASK) >> REG1_OFFSET;
		reg2 = (instruction & REG2_MASK) >> REG2_OFFSET;
		imm = (instruction & IMM_MASK) >> IMM_OFFSET;
#ifdef ALL_INSTRUCTIONS_ARE_CONDITIONAL
		switch (condition){
			case 0:
				break;
			case COND_EQ:
				if (!GETFLAG(FLAG_Z))
					continue;
				break;
			case COND_NE:
				if (GETFLAG(FLAG_Z))
					continue;
				break;
			case COND_LT:
				if (GETFLAG(FLAG_N) == GETFLAG(FLAG_V))
					continue;
				break;
			case COND_LE:
				if (GETFLAG(FLAG_Z) == 0 && GETFLAG(FLAG_N) == GETFLAG(FLAG_V))
					continue;
				break;
			case COND_GE:
				if (GETFLAG(FLAG_N) != GETFLAG(FLAG_V))
					continue;
				break;
			case COND_GT:
				if (GETFLAG(FLAG_Z) != 0 || GETFLAG(FLAG_N) != GETFLAG(FLAG_V))
					continue;
				break;
		}
#endif
		if (mod == 1)
			imm = registers[imm];
		if (mod == 2)
			imm += registers[reg2];
		switch(opcode){
			case INS_ADD:
				registers[reg1] = registers[reg2] + imm;
				break;
			case INS_SUB:
				registers[reg1] = registers[reg2] - imm;
				break;
			case INS_MUL:
				registers[reg1] = registers[reg2] * imm;
				break;
			case INS_DIV:
				registers[reg1] = registers[reg2] / imm;
				break;
			case INS_REM:
				registers[reg1] = registers[reg2] % imm;
				break;
			case INS_AND:
				registers[reg1] = registers[reg2] & imm;
				break;
			case INS_ORR:
				registers[reg1] = registers[reg2] | imm;
				break;
			case INS_EOR:
				registers[reg1] = registers[reg2] ^ imm;
				break;
			case INS_LSL:
				registers[reg1] = (unsigned int) registers[reg2] << imm;
				break;
			case INS_LSR:
				registers[reg1] = (unsigned int) registers[reg2] >> imm;
				break;
			case INS_LDR:
				registers[reg1] = memory[imm];
				break;
			case INS_STR:
				memory[imm] = registers[reg1];
				break;
			case INS_MOV:
				registers[reg1] = imm;
				break;
			case INS_MVN:
				registers[reg1] = ~imm;
				break;
			case INS_B:
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
				switch (condition){
					case 0:
						break;
					case COND_EQ:
						if (!GETFLAG(FLAG_Z))
							continue;
						break;
					case COND_NE:
						if (GETFLAG(FLAG_Z))
							continue;
						break;
					case COND_LT:
						if (GETFLAG(FLAG_N) == GETFLAG(FLAG_V))
							continue;
						break;
					case COND_LE:
						if (GETFLAG(FLAG_Z) == 0 && GETFLAG(FLAG_N) == GETFLAG(FLAG_V))
							continue;
						break;
					case COND_GE:
						if (GETFLAG(FLAG_N) != GETFLAG(FLAG_V))
							continue;
						break;
					case COND_GT:
						if (GETFLAG(FLAG_Z) != 0 || GETFLAG(FLAG_N) != GETFLAG(FLAG_V))
							continue;
						break;
				}
#endif
				pc = imm;
				break;
			case INS_CALL:
				if (stack_index >= stack_size){
					fprintf(stderr, "Stack overflow on instruction %d", pc-1);
					running = 0;
					break;
				}
				stack[stack_index] = pc;
				stack_index++;
				pc = imm;
				break;
			case INS_CMP:
				flags = 0;
				n = registers[reg1] - imm;
				m = registers[reg1] >> 31 & 1;
				b = -imm >> 31 & 1;
				d = n >> 31 & 1;
				if (n == 0)
					flags |= FLAG_Z;
				if (d!=0)
					flags |= FLAG_N;
				if ((!m && b && !d)
						|| (!b && d))
					flags |= FLAG_C;
				else if (b == 0 && d == 1)
					flags |= FLAG_C;
				if(m == b && m != d)
					flags |= FLAG_V; // V Flag
				break;
			case INS_HALT:
				running = 0;
				break;
			case INS_RET:
				stack_index--;
				pc = stack[stack_index];
				break;
			case INS_PUSH:
				stack[stack_index] = registers[reg1];
				stack_index++;
				break;
			case INS_POP:
				stack_index--;
				registers[reg1] = stack[stack_index];
				break;
			case INS_INP:
				switch(imm){
					case 2:
						printf("Please enter a number\n");
						while (!scanf("%d",&(registers[reg1])));
						break;
					case 8:
						registers[reg1] = rand();
						break;
				}
				break;
			case INS_OUT:
				switch(imm){
					case 4:
						format = "%d\n"; // as signed int
						printf(format,registers[reg1]);
						break;
					case 5:
						format = "%u\n"; // as unsigned int
						printf(format,registers[reg1]);
						break;
					case 6:
						format = "%08lx\n"; // as hexadecimal
						printf(format,registers[reg1]);
						break;
					case 7:
						format = "%c\n"; // as ascii character
						printf(format,registers[reg1]);
						break;
					case 8:
						format = "%s\n";
						printf(format,registers[reg1]);
						break;
					default:
						fprintf(stderr, "error on instruction %d: unknown type for `out` instruction: %d", pc-1, imm);
						running = 0;
						break;
				}
				break;
			default:
				fprintf(stderr, "Illegal instruction on address %d: 0x%016lx\n", pc-1, instruction);
				running = 0;
				break;
		}
	}
	return 0;
}
