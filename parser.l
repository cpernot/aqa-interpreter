%{
#include "bison.h"
#include "aqa.h"
#include "config.h"

#ifdef DEBUG
#define RETURN(tok) fprintf(stderr, "%s -> "#tok"\n", yytext);return tok;
#else
#define RETURN(tok) return tok;
#endif

char getcondition(char *str){
	if (*str == '\0')
		return 0;
	if (!strcmp(str,"eq"))
		return COND_EQ;
	if (!strcmp(str,"ne"))
		return COND_NE;
	if (!strcmp(str,"lt"))
		return COND_LT;
	if (!strcmp(str,"le"))
		return COND_LE;
	if (!strcmp(str,"gt"))
		return COND_GT;
	if (!strcmp(str,"ge"))
		return COND_GE;
	fprintf(stderr, "unknown condition: %s\n",str);
	return 0;
}

%}
%option noyywrap
%option caseless

condition (eq|ne|lt|le|gt|ge)
%%
[ \t] {} //pass
\/\/.*$ {} //pass
\n {RETURN (TOK_NEWLINE);}
; {RETURN (TOK_SEMICOLON);}
, {RETURN (TOK_SEP);}
: {RETURN (TOK_COLON);}
\. {RETURN (TOK_DOT);}
# {RETURN (TOK_NUMERIC_PREFIX);}
\+ {RETURN (TOK_PLUS);}
\- {RETURN (TOK_MINUS);}
r[0-9]+ {yylval.number = atoi(yytext+1); RETURN (TOK_REG);}
\[ {RETURN (TOK_BRACKET_OPEN);}
\] {RETURN (TOK_BRACKET_CLOSE);}
dat {RETURN (TOK_DATA);}
and{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("and"));
	yylval.action.opcode = INS_AND;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
orr{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("orr"));
	yylval.action.opcode = INS_ORR;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
eor{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("eor"));
	yylval.action.opcode = INS_EOR;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
add{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("add"));
	yylval.action.opcode = INS_ADD;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
sub{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("sub"));
	yylval.action.opcode = INS_SUB;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
mul{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("mul"));
	yylval.action.opcode = INS_MUL;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
div{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("div"));
	yylval.action.opcode = INS_DIV;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
rem{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("rem"));
	yylval.action.opcode = INS_REM;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
lsl{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("lsl"));
	yylval.action.opcode = INS_LSL;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
lsr{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("lsr"));
	yylval.action.opcode = INS_LSR;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_REG_OP);
}
mov{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("mov"));
	yylval.action.opcode = INS_MOV;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_OP);
}
mvn{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("mvn"));
	yylval.action.opcode = INS_MVN;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_OP);
}
ldr{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("ldr"));
	yylval.action.opcode = INS_LDR;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_ADDR);
}
str{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("str"));
	yylval.action.opcode = INS_STR;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_ADDR);
}
inp{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("inp"));
	yylval.action.opcode = INS_INP;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_NUM);
}
out{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("out"));
	yylval.action.opcode = INS_OUT;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_NUM);
}
push{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("push"));
	yylval.action.opcode = INS_PUSH;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG);
}
pop{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("pop"));
	yylval.action.opcode = INS_POP;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG);
}
cmp{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("cmp"));
	yylval.action.opcode = INS_CMP;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_REG_OP);
}
b{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("b"));
	yylval.action.opcode = INS_B;
	RETURN (TOK_INS_ADDR);
}
call{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("call"));
	yylval.action.opcode = INS_CALL;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_ADDR);
}
ret{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("ret"));
	yylval.action.opcode = INS_RET;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_NO_ARG);
}
halt{condition}? {
	yylval.action.condition = getcondition(yytext+strlen("halt"));
	yylval.action.opcode = INS_HALT;
#ifndef ALL_INSTRUCTIONS_ARE_CONDITIONAL
	if (yylval.action.condition)
		yyerror("can't use a condition in this instruction");
#endif
	RETURN (TOK_INS_NO_ARG);
}
-?[0-9]+ {yylval.number = atol(yytext); RETURN (TOK_NUMERIC);}
0x[0-9a-f]+ {yylval.number = strtoull(yytext,NULL,16); RETURN (TOK_NUMERIC);}
[a-z][a-z0-9]* {yylval.str = strdup(yytext); RETURN (TOK_LABEL);}
